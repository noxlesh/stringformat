﻿using System;
using System.Globalization;
using System.Threading;

namespace StringFormats
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            decimal m = 15.7m;
            double d = 16.560339334;
            char c = '3';
            
            Thread.CurrentThread.CurrentCulture = new CultureInfo("uk-ua");
            
            string s =  String.Format("Currency: \t\t{0:C}", m);
            Console.WriteLine(s);
            
            s = String.Format("Double to fixed: \t{0:F}", d);
            Console.WriteLine(s);
            
            s = String.Format("Double to fixed 5:  \t{0:F5}", d);
            Console.WriteLine(s);
            
            s = String.Format("Decimal to fixed 1: \t{0:N1}", m);
            Console.WriteLine(s);

            s = String.Format("Char \'{0}\' hex repr.: \t0x{1:X4}", c, (int)c);
            Console.WriteLine(s);

            s = String.Format("Today date is: \t\t{0:d}", DateTime.Today);
            Console.WriteLine(s);

            s = String.Format("Current time is: \t{0:t}", DateTime.Now);
            Console.WriteLine(s);


        }
    }
}